package com.agred.alcoludo.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import com.agred.alcoludo.Constants;
import com.agred.alcoludo.entities.Pawn;
import com.agred.alcoludo.entities.Player;
import com.agred.alcoludo.entities.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by agred on 15.05.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Databases Names
    private static final String DATABASE_NAME = "database";

    // Tables names
    private static final String TABLE_PLAYERS = "players";
    private static final String TABLE_TASKS = "tasks";
    private static final String TABLE_TURN = "turn";

    // Players table columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FIELD = "field";
    private static final String KEY_NAME = "name";
    private static final String KEY_COLOR = "color";
    private static final String KEY_PAWN = "pawn";
    private static final String KEY_TURN = "turn";
    private static final String KEY_STATS = "stats";

    // Tasks table columns names
    private static final String KEY_TASK = "task";
    private static final String KEY_LOCALE = "locale";
    private static final String KEY_CODE = "code";
    private static final String KEY_ATTRIBUTE = "taskAttribute";

    // Create Players Table
    private static final String CREATE_PLAYERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PLAYERS
            + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_FIELD + " INTEGER,"
            + KEY_NAME + " TEXT,"
            + KEY_COLOR + " TEXT,"
            + KEY_PAWN + " TEXT,"
            + KEY_STATS + " INTEGER)";

    private static final String CREATE_TASKS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TASKS
            + "("
            + KEY_TASK + " TEXT,"
            + KEY_LOCALE + " TEXT,"
            + KEY_CODE + " TEXT,"
            + KEY_ATTRIBUTE + " TEXT)";

    private static final String CREATE_TURN_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TURN
            + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_TURN + " INTEGER)";

    private static DatabaseHelper helper = null;
    private Context myContext;

    //Constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }

    public static void init(Context context) {
        helper = new DatabaseHelper(context);
    }

    public static DatabaseHelper getDbHelper() {
        return helper;
    }

    // General tables operations
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PLAYERS_TABLE);
        db.execSQL(CREATE_TURN_TABLE);
        db.execSQL(CREATE_TASKS_TABLE);
        populateTasksTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older tables if exist
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TURN);
        // Create tables again
        db.execSQL(CREATE_PLAYERS_TABLE);
        db.execSQL(CREATE_TURN_TABLE);
    }

    public void resetGame() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TURN);
        db.execSQL(CREATE_PLAYERS_TABLE);
        db.execSQL(CREATE_TURN_TABLE);
        db.close();
    }

    //Players table operations
    public void addPlayer(Player p, Pawn pawn) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_ID, p.getId());
        cv.put(KEY_NAME, p.getName());
        cv.put(KEY_FIELD, p.getField());
        cv.put(KEY_COLOR, p.getColor());
        cv.put(KEY_STATS, p.getStats());
        cv.put(KEY_PAWN, pawn.getColor());

        db.insert(TABLE_PLAYERS, null, cv);
        db.close();
    }

    public String getPlayerName(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PLAYERS, new String[]{KEY_NAME}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst())
            cursor.moveToFirst();

        String p = cursor.getString(cursor.getColumnIndex(KEY_NAME));
        cursor.close();
        return p;
    }

    public int getInt(int id, String table, String key) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(table, new String[]{key}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst())
            cursor.moveToFirst();

        int p = cursor.getInt(cursor.getColumnIndex(key));
        cursor.close();
        return p;
    }

    public int updateField(Player p, int result) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();
        p.setField(result);
        values.put(KEY_FIELD, result);

        // updating row
        return db.update(TABLE_PLAYERS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(p.getId())});
    }

    public int getPlayersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_PLAYERS;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);
        int a = cursor.getCount();
        cursor.close();

        return a;
    }

    public int updateKeyTurn(int playerNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_TURN, playerNumber);

        return db.update(TABLE_TURN, cv, KEY_ID + " = ?",
                new String[]{String.valueOf(1)});
    }

    //Turn table operations
    public void setKeyTurn(int playerNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_TURN, playerNumber);

        db.insert(TABLE_TURN, null, cv);
        db.close();
    }

    //Tasks table operations
    private void populateTasksTable(SQLiteDatabase db) {
        InputStream insertsStream;
        BufferedReader insertReader;
        try {
            insertsStream = myContext.getAssets().open("tasks.sql");
            insertReader = new BufferedReader(new InputStreamReader(insertsStream));
            while (insertReader.ready()) {
                String insertStmt = insertReader.readLine();
                db.execSQL(insertStmt);
            }
            insertReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Task> getTasksOfLocale(String locale) {
        int gameLength = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(myContext).getString(Constants.gameLength, "69"));
        ArrayList<Task> taskList = new ArrayList<>(gameLength);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_TASKS, new String[]{KEY_TASK, KEY_LOCALE, KEY_CODE, KEY_ATTRIBUTE}, KEY_LOCALE + " =? ",
                new String[]{locale}, null, null, null, null);
        if (cursor == null)
            return null;
        if (cursor.moveToFirst())
            fillTasksList(locale, taskList, cursor);
        if(taskList.size() < gameLength)
            fillTasksList(locale, taskList, cursor);
        cursor.close();
        System.out.println("Tasks count: " + taskList.size());
        return taskList;
    }

    private void fillTasksList(String locale, ArrayList<Task> taskList, Cursor cursor) {
        for (int i = 0; i < getTasksOfLocaleCount(locale); ++i) {
            taskList.add(new Task(
                    cursor.getString(cursor.getColumnIndex(KEY_TASK)),
                    cursor.getString(cursor.getColumnIndex(KEY_LOCALE)),
                    cursor.getString(cursor.getColumnIndex(KEY_CODE)),
                    cursor.getString(cursor.getColumnIndex(KEY_ATTRIBUTE))
            ));
            if (!cursor.moveToNext())
                cursor.moveToFirst();
        }
    }

    public void addTasks(ArrayList<Task> taskList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        for (int i = 0; i < taskList.size(); ++i) {
            cv.put(KEY_TASK, taskList.get(i).getTask());
            cv.put(KEY_LOCALE, taskList.get(i).getLocale());
            cv.put(KEY_CODE, taskList.get(i).getCode());
            cv.put(KEY_ATTRIBUTE, taskList.get(i).getTaskAttribute());
            db.insert(TABLE_TASKS, null, cv);
        }
        db.close();
    }

    public void clearTasksTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE " + TABLE_TASKS);
        db.execSQL(CREATE_TASKS_TABLE);
    }

    public long getTasksCount() {
        return DatabaseUtils.queryNumEntries(getReadableDatabase(), TABLE_TASKS);
    }

    private long getTasksOfLocaleCount(String locale) {
        return DatabaseUtils.queryNumEntries(getReadableDatabase(), TABLE_TASKS, KEY_LOCALE + " =? ",
                new String[]{locale});
    }

}
