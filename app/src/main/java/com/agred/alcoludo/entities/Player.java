package com.agred.alcoludo.entities;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by agred on 10.05.2016.
 */
public class Player {
    private int id;
    private int field;
    private String name;
    private int color;
    private Pawn pawn;
    private boolean passTurn;
    private int stats;

    public Player(int id, int field, String name, int color, Pawn pawn, int stats) {
        this.id = id;
        this.field = field;
        this.name = name;
        this.color = color;
        this.pawn = pawn;
        this.stats = stats;
    }

    public int getId() {
        return id;
    }

    public int getMaxField(int playerCount, ArrayList<Player> playerList) {
        ArrayList<Integer> playersFields = new ArrayList<>(playerCount);
        for (int i = 0; i < playerCount; ++i) {
            playersFields.add(playerList.get(i % playerCount).getField());
        }
        return Collections.max(playersFields);
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    public Pawn getPawn() {
        return pawn;
    }

    public boolean isPassTurn() {
        return passTurn;
    }

    public void setPassTurn(boolean passTurn) {
        this.passTurn = passTurn;
    }

    public int getStats() {
        return stats;
    }
}
