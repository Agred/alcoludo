package com.agred.alcoludo.entities;

import java.util.Random;

/**
 * Created by agred on 10.05.2016.
 */

public class Dice extends Random {

    //number is amount of dices, nSides is amount of sides on one dice. for example 1 dice 6 sides (1k6) is rollDice(1, 6)
    public static int rollDice(int number, int nSides) {
        int num = 0;
        int roll = 0;
        Random r = new Random();
        for (int i = 0; i < number; i++) {
            roll = r.nextInt(nSides) + 1;
            num = num + roll;
        }
        return num;
    }
}
