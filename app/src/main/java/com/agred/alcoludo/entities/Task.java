package com.agred.alcoludo.entities;

/**
 * Created by agred on 17.05.2016.
 */
public class Task {

    private String task;
    private String locale;
    private String code;
    private String taskAttribute;


    public Task(String task, String locale, String code, String taskAttribute) {
        this.task = task;
        this.locale = locale;
        this.code = code;
        this.taskAttribute = taskAttribute;
    }

    public String getTask() {
        return task;
    }

    public String getLocale() {
        return locale;
    }

    public String getCode() {
        return code;
    }

    public String getTaskAttribute() {
        return taskAttribute;
    }
}
