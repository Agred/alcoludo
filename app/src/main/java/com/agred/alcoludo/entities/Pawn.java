package com.agred.alcoludo.entities;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.agred.alcoludo.R;
import com.agred.alcoludo.helpers.DatabaseHelper;
import com.agred.alcoludo.ui.activities.GameActivity;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;

/**
 * Created by agred on 13.06.2016.
 */
public class Pawn {
    private int color;

    public Pawn(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public View createPawn(ArrayList<Player> playerList, GridLayout boardSpawn, int playerNumber, int playerCount, Context context) {
        int color = playerList.get(playerNumber % playerCount).getPawn().getColor();

        LayoutInflater pawnInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View pawn = pawnInflater.inflate(R.layout.single_pawn, boardSpawn, false);
        GradientDrawable circle = (GradientDrawable) pawn.getBackground();
        circle.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        float fieldWidth = GameActivity.fieldWidth / 2;
        float fieldHeight = GameActivity.fieldHeight / 3;
        if (fieldWidth > fieldHeight)
            pawn.setLayoutParams(new ViewGroup.LayoutParams(Math.round(fieldHeight), Math.round(fieldHeight)));
        else
            pawn.setLayoutParams(new ViewGroup.LayoutParams(Math.round(fieldWidth), Math.round(fieldWidth)));

        return pawn;
    }

    public void addPawn(ArrayList<Player> playerList, GridLayout boardSpawn, int playerNumber, int playerCount, View pawn) {
        int playerField = playerList.get(playerNumber % playerCount).getField();

        FlowLayout oldParent = (FlowLayout) pawn.getParent();
        if (oldParent != null)
            oldParent.removeView(pawn);

        FlowLayout spawn = (FlowLayout) boardSpawn.getChildAt(playerField);
        if (spawn != null) {
            spawn.addView(pawn);
            Log.d("Added pawn of player", String.valueOf((playerNumber % playerCount)));
        } else
            Log.e("LinearLayout spawn", "null!");
    }

    public void removePawn(ArrayList<Player> playerList, GridLayout boardSpawn, int playerNumber, int playerCount, View pawn) {
        int playerField = playerList.get(playerNumber % playerCount).getField();

        FlowLayout oldParent = (FlowLayout) pawn.getParent();
        if (oldParent != null)
            oldParent.removeView(pawn);
        else {
            FlowLayout spawn = (FlowLayout) boardSpawn.getChildAt(playerField);
            if (spawn != null) {
                spawn.removeViewAt(0);
                Log.d("Deleted pawn of player", String.valueOf((playerNumber % playerCount)));
            } else
                Log.e("LinearLayout spawn", "null!");
        }
    }

    public void movePawn(ArrayList<Player> playerList, GridLayout boardSpawn, TextView fieldDisplay, View pawn, int playerNumber, int playerCount, int result) {
        removePawn(playerList, boardSpawn, playerNumber, playerCount, pawn);

        DatabaseHelper.getDbHelper().updateField(playerList.get(playerNumber % playerCount), result);
        playerList.get(playerNumber % playerCount).setField(DatabaseHelper.getDbHelper().getInt(playerNumber % playerCount, "players", "field"));
        fieldDisplay.setText(String.valueOf(playerList.get(playerNumber % playerCount).getField()));

        addPawn(playerList, boardSpawn, playerNumber, playerCount, pawn);
    }
}
