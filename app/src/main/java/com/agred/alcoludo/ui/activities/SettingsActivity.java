package com.agred.alcoludo.ui.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.WindowManager;

import com.agred.alcoludo.R;

/**
 * Created by agred on 10.05.2016.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settingsprefs);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }
}
