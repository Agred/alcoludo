package com.agred.alcoludo.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.Button;

import com.agred.alcoludo.R;

/**
 * Created by agred on 17.08.2016.
 */
public class AppRateDialog {
    private final static String APP_PNAME = "com.agred.alcoludo";

    private final static int LAUNCHES_UNTIL_PROMPT = 2;

    public static void appLaunchedCounter(Context mContext) {
        SharedPreferences sp = mContext.getSharedPreferences("appRater", 0);
        if (sp.getBoolean("dontShowAgain", false))
            return;

        SharedPreferences.Editor editor = sp.edit();

        // Increment launch counter
        long launchCount = sp.getLong("launchCount", 0) + 1;
        editor.putLong("launchCount", launchCount);

        // Get date of first launch
        Long date_firstLaunch = sp.getLong("firstLaunchDate", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("firstLaunchDate", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launchCount >= LAUNCHES_UNTIL_PROMPT) {
            showRateDialog(mContext, editor);
            editor.putLong("launchCount", 0);
        }

        editor.apply();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog rateDialog = new Dialog(mContext);
        rateDialog.setContentView(R.layout.custom_dialog);
        Button rateBtn = (Button) rateDialog.findViewById(R.id.rateBtn);
        Button dismissBtn = (Button) rateDialog.findViewById(R.id.dismissBtn);
        Button cancelBtn = (Button) rateDialog.findViewById(R.id.cancelBtn);

        rateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontShowAgain", true);
                    editor.commit();
                }
                rateDialog.dismiss();
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
            }
        });

        dismissBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rateDialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontShowAgain", true);
                    editor.commit();
                }
                rateDialog.dismiss();
            }
        });

        rateDialog.show();
    }
}