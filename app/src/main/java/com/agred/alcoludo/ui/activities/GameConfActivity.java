package com.agred.alcoludo.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import com.agred.alcoludo.R;
import com.agred.alcoludo.entities.Pawn;
import com.agred.alcoludo.entities.Player;
import com.agred.alcoludo.helpers.DatabaseHelper;

import org.xdty.preference.colorpicker.ColorPickerDialog;
import org.xdty.preference.colorpicker.ColorPickerSwatch;

/**
 * Created by agred on 10.05.2016.
 */
public class GameConfActivity extends AppCompatActivity {
    private boolean playersReady = true;
    private int selectedColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_conf);
        if (getResources().getBoolean(R.bool.portrait_only))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        DatabaseHelper.getDbHelper().resetGame();
        initNumberPicker();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.toolbar_gameconf);
        NumberPicker nPicker = (NumberPicker) findViewById(R.id.playerNum);
        LinearLayout playersLayout = (LinearLayout) findViewById(R.id.playersListLayout);
        FloatingActionButton playBtn = (FloatingActionButton) findViewById(R.id.gameBtn);
        RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.player1);
        RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.player2);
        if (layout1 != null) {
            Button colorBtn = (Button) layout1.findViewById(R.id.colorBtn);
            chooseColor(colorBtn);
        }
        if (layout2 != null) {
            Button colorBtn2 = (Button) layout2.findViewById(R.id.colorBtn);
            chooseColor(colorBtn2);
        }
        playersConf(nPicker, playersLayout);
        startGame(playBtn, playersLayout);
    }

    private void initNumberPicker(){
        NumberPicker nPicker = (NumberPicker) findViewById(R.id.playerNum);
        if(nPicker == null)
            return;
        nPicker.setValue(2);
        nPicker.setMinValue(2);
        nPicker.setMaxValue(8);
        nPicker.setWrapSelectorWheel(false);
    }

    private AlertDialog playerErrorDialogConf() {
        AlertDialog.Builder playerErrorDialog;
        playerErrorDialog = new AlertDialog.Builder(GameConfActivity.this, R.style.DialogStyle);
        playerErrorDialog.setTitle(R.string.error).setMessage(R.string.playersNamesError);
        playerErrorDialog.setCancelable(false);
        playerErrorDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface playerErrorDialog, int which) {

            }
        });
        return playerErrorDialog.create();
    }

    private void chooseColor(final Button colorBtn) {
        colorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedColor = ContextCompat.getColor(GameConfActivity.this, R.color.blue);
                int[] mColors = getResources().getIntArray(R.array.default_rainbow);

                ColorPickerDialog dialog = ColorPickerDialog.newInstance(R.string.color_picker_default_title,
                        mColors,
                        selectedColor,
                        4, // Number of columns
                        ColorPickerDialog.SIZE_SMALL);

                dialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int color) {
                        selectedColor = color;
                        colorBtn.setTextColor(selectedColor);
                    }
                });
                dialog.show(getFragmentManager(), "color_dialog_test");
            }
        });
    }

    private void playersConf(NumberPicker nPicker, final LinearLayout playersLayout) {
        nPicker.setOnValueChangedListener((new NumberPicker.OnValueChangeListener() {

            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                String[] oldValues = new String[playersLayout.getChildCount()];
                int[] oldColors = new int[playersLayout.getChildCount()];
                for (int i = 0; i < playersLayout.getChildCount(); i++) {
                    oldValues[i] = ((EditText) playersLayout.getChildAt(i).findViewById(R.id.playerName)).getText().toString();
                    oldColors[i] = ((Button) playersLayout.getChildAt(i).findViewById(R.id.colorBtn)).getCurrentTextColor();
                }
                playersLayout.removeAllViews();
                for (int i = 0; i < newVal; ++i) {
                    final View spInflater = getLayoutInflater().inflate(R.layout.single_player_view, null);
                    Button colorBtn = (Button) spInflater.findViewById(R.id.colorBtn);
                    chooseColor(colorBtn);
                    if (i < oldValues.length) {
                        EditText ed = (EditText) spInflater.findViewById(R.id.playerName);
                        ed.setText(oldValues[i]);
                        Button btn = (Button) spInflater.findViewById(R.id.colorBtn);
                        btn.setTextColor(oldColors[i]);
                    }
                    playersLayout.addView(spInflater);
                }
            }
        }));
    }

    private void savePlayers(final LinearLayout playersLayout) {
        for (int i = 0; i < playersLayout.getChildCount(); i++) {
            EditText nameField = (EditText) playersLayout.getChildAt(i).findViewById(R.id.playerName);
            Button colorBtn = (Button) playersLayout.getChildAt(i).findViewById(R.id.colorBtn);
            String name = nameField.getText().toString().trim();
            int color = colorBtn.getCurrentTextColor();
            int stats = 0;
            if (!name.isEmpty()) {
                Pawn pawn = new Pawn(color);
                Player p = new Player(i, 0, name, color, pawn, stats);
                DatabaseHelper.getDbHelper().addPlayer(p, pawn);
                Log.i("Registered new player: ", String.valueOf(p.getName()));
            } else
                playersReady = false;
        }
    }

    private void startGame(android.support.design.widget.FloatingActionButton playBtn, final LinearLayout playersLayout) {
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playersReady = true;
                savePlayers(playersLayout);
                if (playersReady) {
                    Intent intent = new Intent(GameConfActivity.this, GameActivity.class);
                    intent.putExtra("playerNumber", 0);
                    finish();
                    startActivity(intent);
                } else
                    playerErrorDialogConf().show();
            }
        });
    }
}
