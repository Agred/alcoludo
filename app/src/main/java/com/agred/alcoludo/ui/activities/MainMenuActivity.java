package com.agred.alcoludo.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.agred.alcoludo.Constants;
import com.agred.alcoludo.R;
import com.agred.alcoludo.helpers.DatabaseHelper;
import com.agred.alcoludo.helpers.LocaleHelper;
import com.agred.alcoludo.service.ApiService;
import com.agred.alcoludo.ui.AppRateDialog;
import com.jakewharton.processphoenix.ProcessPhoenix;

/**
 * Created by agred on 10.05.2016.
 */
public class MainMenuActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        LocaleHelper.onCreate(getBaseContext(), getResources().getConfiguration().locale.getLanguage());
        DatabaseHelper.init(this);
        new ApiService(this, ApiService.ACTION_GET_TRANSLATIONS_COUNT).execute();
        AppRateDialog.appLaunchedCounter(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.toolbar_mainmenu);
        if (getResources().getBoolean(R.bool.portrait_only))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initSettingsBtn();
        initPlayBtn();
        initContinueBtn();
        initCreditsBtn();
    }

    private AlertDialog newGameErrorAlertConf() {
        final AlertDialog.Builder newGameErrorDialog;
        newGameErrorDialog = new AlertDialog.Builder(MainMenuActivity.this);
        newGameErrorDialog.setTitle(R.string.warning).setMessage(R.string.newGameWarning);
        newGameErrorDialog.setCancelable(false);
        newGameErrorDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface playerErrorDialog, int which) {
                startActivity(new Intent(MainMenuActivity.this, GameConfActivity.class));
            }
        });
        newGameErrorDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return newGameErrorDialog.create();
    }

    private void initSettingsBtn() {
        Button settingsBtn = findViewById(R.id.settingsBtn);
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, SettingsActivity.class));
            }
        });
    }

    private void initPlayBtn() {
        Button playBtn = findViewById(R.id.playBtn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DatabaseHelper.getDbHelper().getPlayersCount() > 0)
                    newGameErrorAlertConf().show();
                else
                    startActivity(new Intent(MainMenuActivity.this, GameConfActivity.class));
            }
        });
    }

    private void initContinueBtn() {
        Button continueBtn = findViewById(R.id.continueBtn);
        if (DatabaseHelper.getDbHelper().getPlayersCount() > 0) {
            continueBtn.setEnabled(true);
            continueBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent gameContinue = new Intent(MainMenuActivity.this, GameActivity.class);
                    gameContinue.putExtra(Constants.isGameContinued, true);
                    startActivity(gameContinue);
                }
            });
        } else
            continueBtn.setEnabled(false);
    }

    private void initCreditsBtn() {
        Button creditsBtn = findViewById(R.id.creditsBtn);
        creditsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, CreditsActivity.class));
            }
        });
    }

    public void changeLanguage(SharedPreferences sp) {
        String language = sp.getString(Constants.language, getResources().getConfiguration().locale.getLanguage());
        Log.d("App locale", language);
        LocaleHelper.setLocale(getBaseContext(), language);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        if (key.equals(Constants.language)) {
            changeLanguage(sp);
            ProcessPhoenix.triggerRebirth(getBaseContext());
        } else if (key.equals(Constants.gameLength) || key.equals(Constants.diceNumber) || key.equals(Constants.diceShape))
            DatabaseHelper.getDbHelper().resetGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initContinueBtn();
    }
}