package com.agred.alcoludo.ui.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agred.alcoludo.Constants;
import com.agred.alcoludo.R;
import com.agred.alcoludo.entities.Dice;
import com.agred.alcoludo.entities.Pawn;
import com.agred.alcoludo.entities.Player;
import com.agred.alcoludo.entities.Task;
import com.agred.alcoludo.helpers.DatabaseHelper;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;


/**
 * Created by agred on 10.05.2016.
 */
public class GameActivity extends AppCompatActivity {

    public static float fieldWidth;
    public static float fieldHeight;
    public static int gameLength;
    ArrayList<Player> playerList;
    ArrayList<String> playersNames;
    private boolean playerChange = false;
    private boolean isHorizontal;
    private int playerNumber = 0;
    private int playerCount = DatabaseHelper.getDbHelper().getPlayersCount();
    private int boardWidth;
    private int boardHeight;

    // Code readability methods
    private int playerField(int playerNumber) {
        return DatabaseHelper.getDbHelper().getInt(playerNumber % playerCount, Constants.players, Constants.field);
    }

    private String playerName(int playerNumber) {
        return DatabaseHelper.getDbHelper().getPlayerName(playerNumber);
    }

    private int playerColor(int playerNumber) {
        return DatabaseHelper.getDbHelper().getInt(playerNumber, Constants.players, Constants.color);
    }

    private int pawnColor(int playerNumber) {
        return DatabaseHelper.getDbHelper().getInt(playerNumber, Constants.players, Constants.pawn);
    }

    private int playerStats(int playerNumber) {
        return DatabaseHelper.getDbHelper().getInt(playerNumber, Constants.players, Constants.stats);
    }
    // End of Code readability methods

    // Game specific methods
    private void populatePlayers(int playerCount, ArrayList<Player> playerList) {
        for (int i = 0; i < playerCount; ++i) {
            playerList.add(new Player(i, playerField(i), playerName(i), playerColor(i), new Pawn(pawnColor(i)), playerStats(i)));
            Log.i("Added player to list: ", playerName(i));
        }
    }

    private void randomize(ArrayList<Task> tasks) {
        long seed = System.nanoTime();
        Collections.shuffle(tasks, new Random(seed));
    }

    private void showTask(ArrayList<Task> taskList, ArrayList<Player> playerList, TextView fieldDisplay, GridLayout boardSpawn, View pawn) {
        int currentField = playerList.get(playerNumber % playerCount).getField();
        String taskDesc = taskList.get(currentField).getTask();

        taskActions(taskList, playerList, fieldDisplay, boardSpawn, pawn);

        AlertDialog.Builder taskDialog = new AlertDialog.Builder(GameActivity.this);
        taskDialog.setTitle(R.string.task).setMessage(taskDesc);
        taskDialog.setCancelable(false);
        taskDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        taskDialog.show();
    }

    private void taskActions(ArrayList<Task> taskList, ArrayList<Player> playerList, TextView fieldDisplay, GridLayout boardSpawn, View pawn) {
        int currentField = playerList.get(playerNumber % playerCount).getField();
        Pawn p = playerList.get(playerNumber % playerCount).getPawn();

        if (taskList.get(currentField).getCode() != null) {
            String tileCode = taskList.get(currentField).getCode();
            Log.d("Registered fieldCode", String.valueOf(tileCode));
            int codeField;

            switch (tileCode) {
                case "goBack":
                    codeField = Integer.valueOf(taskList.get(currentField).getTaskAttribute());
                    if (currentField > codeField)
                        p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, (currentField - codeField));
                    else
                        p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, 0);
                    break;

                case "goTo":
                    if (!taskList.get(currentField).getTaskAttribute().equals("start")) {
                        codeField = Integer.valueOf(taskList.get(currentField).getTaskAttribute());
                        p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, codeField);
                    } else
                        p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, 0);
                    break;

                case "everyoneGoBack":
                    for (int i = 0; i < playerCount; ++i) {
                        int currField = playerList.get(i).getField();
                        if (currField > 0) {
                            Pawn iP = playerList.get(i % playerCount).getPawn();
                            View iPawn = iP.createPawn(playerList, boardSpawn, i, playerCount, GameActivity.this);
                            p.movePawn(playerList, boardSpawn, fieldDisplay, iPawn, i, playerCount, (currField - 1));
                        } else {
                            Pawn iP = playerList.get(i % playerCount).getPawn();
                            View iPawn = iP.createPawn(playerList, boardSpawn, i, playerCount, GameActivity.this);
                            p.movePawn(playerList, boardSpawn, fieldDisplay, iPawn, i, playerCount, 0);
                        }
                    }
                    break;

                case "passTurn":
                    playerList.get(playerNumber % playerCount).setPassTurn(true);
                    break;

                case "chooseGoTo":
                    codeField = Integer.valueOf(taskList.get(currentField).getTaskAttribute());
                    chooseSinglePlayer(fieldDisplay, boardSpawn, codeField).show();
                    break;

                case "swapPlaces":
                    int maxField = playerList.get(playerNumber % playerCount).getMaxField(playerCount, playerList);
                    p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, maxField);
                    break;

                case "timer":
                    codeField = Integer.valueOf(taskList.get(currentField).getTaskAttribute());
                    final AlertDialog timerDialog = new AlertDialog.Builder(this).create();
                    timerDialog.setTitle(R.string.timer_title);
                    timerDialog.setMessage(String.valueOf(codeField));
                    timerDialog.setCancelable(false);
                    timerDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.skip), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            timerDialog.dismiss();
                        }
                    });
                    timerDialog.show();

                    new CountDownTimer((codeField * 60 * 1000), 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            String time;
                            int seconds = (int) (millisUntilFinished / 1000);
                            int minutes = seconds / 60;
                            seconds = seconds % 60;
                            String tempSec = Integer.toString(seconds);
                            if (tempSec.length() == 1)
                                tempSec = "0" + tempSec;
                            time = getResources().getString(R.string.time_left) + " " + minutes + ":" + tempSec;
                            timerDialog.setMessage(time);
                        }

                        @Override
                        public void onFinish() {
                            timerDialog.dismiss();
                        }
                    }.start();
                    break;
            }
        }
    }

    private AlertDialog chooseSinglePlayer(final TextView fieldDisplay, final GridLayout boardSpawn, final int codeField) {
        playersNames = new ArrayList<>(playerCount);

        for (int i = 0; i < playerCount; ++i) {
            playersNames.add(playerList.get(i % playerCount).getName());
        }
        final CharSequence[] playersNamesSequence = playersNames.toArray(new CharSequence[playersNames.size()]);

        AlertDialog.Builder chooseSinglePlayerDialog = new AlertDialog.Builder(GameActivity.this);
        chooseSinglePlayerDialog.setTitle(R.string.choosePlayer).setItems(playersNamesSequence, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.d("which", String.valueOf(which));
                Pawn p = playerList.get(which % playerCount).getPawn();
                View pawn = p.createPawn(playerList, boardSpawn, which, playerCount, GameActivity.this);
                p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, which, playerCount, codeField);
            }
        });
        chooseSinglePlayerDialog.setCancelable(false);
        return chooseSinglePlayerDialog.create();
    }

    private void play(SharedPreferences sp, ArrayList<Player> playerList, TextView fieldDisplay, ArrayList<Task> taskList, android.support.design.widget.FloatingActionButton diceBtn, TextView diceDisplay, GridLayout boardSpawn, TextView nameDisplay) {
        //Dice throw simulation. Set amount of dices and amount of their sides from SharedPreferences
        int result = Dice.rollDice(Integer.valueOf(sp.getString(Constants.diceNumber, "1")), Integer.valueOf(sp.getString(Constants.diceShape, "6")));
        diceDisplay.setText(String.valueOf(result));
        Pawn p = playerList.get(playerNumber % playerCount).getPawn();
        int playerField = playerList.get(playerNumber % playerCount).getField();

        if (playerList.get(playerNumber % playerCount).isPassTurn()) {
            diceBtn.setImageResource(R.drawable.ic_dice);
            playerList.get(playerNumber % playerCount).setPassTurn(false);
            ++playerNumber;
            DatabaseHelper.getDbHelper().updateKeyTurn(playerNumber % playerCount);
            playerChange = true;
            changePlayers(diceBtn, nameDisplay, fieldDisplay, diceDisplay);
        } else {
            if ((playerField + result) <= gameLength) {
                View pawn = p.createPawn(playerList, boardSpawn, playerNumber, playerCount, GameActivity.this);
                p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, (playerField + result));

                showTask(taskList, playerList, fieldDisplay, boardSpawn, pawn);
            } else {
                View pawn = p.createPawn(playerList, boardSpawn, playerNumber, playerCount, GameActivity.this);
                p.movePawn(playerList, boardSpawn, fieldDisplay, pawn, playerNumber, playerCount, gameLength);
                win(playerList);
            }

            //Change diceBtn text to Done
            diceBtn.setImageResource(R.drawable.ic_done);

            //Increment playerNumber to change the player
            ++playerNumber;
            DatabaseHelper.getDbHelper().updateKeyTurn(playerNumber % playerCount);

            //Workaround to change players after finishing an turn
            playerChange = true;
        }
    }

    private void changePlayers(android.support.design.widget.FloatingActionButton diceBtn, TextView nameDisplay, TextView fieldDisplay, TextView diceDisplay) {
        diceBtn.setImageResource(R.drawable.ic_dice);

        nameDisplay.setText(playerName(playerNumber % playerCount));

        nameDisplay.setTextColor(playerColor(playerNumber % playerCount));

        fieldDisplay.setText(String.valueOf(playerField(playerNumber)));

        diceDisplay.setText("");

        //Needed to let a new player throw a dice
        playerChange = false;
    }

    private void win(ArrayList<Player> playerList) {
        String winnerText = getResources().getString(R.string.winner);
        String winnerName = playerList.get(playerNumber % playerCount).getName();
        String msg = winnerText + " " + winnerName;
        AlertDialog.Builder winnerDialog = new AlertDialog.Builder(GameActivity.this);
        winnerDialog.setTitle(R.string.end).setMessage(msg);
        winnerDialog.setCancelable(false);
        winnerDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                DatabaseHelper.getDbHelper().resetGame();
            }
        });
        winnerDialog.show();
    }
    // End of game specific methods

    private void drawBoard() {
        final GridLayout board = findViewById(R.id.board);
        final GridLayout boardSpawn = findViewById(R.id.boardSpawn);

        OrientationEventListener oel = new OrientationEventListener(GameActivity.this) {
            @Override
            public void onOrientationChanged(int orientation) {
                Configuration Config = getResources().getConfiguration();
                if (Config.orientation == Configuration.ORIENTATION_LANDSCAPE)
                    isHorizontal = true;
                else if (Config.orientation == Configuration.ORIENTATION_PORTRAIT)
                    isHorizontal = false;
            }
        };
        oel.enable();

        final int column = gameLength / 10;
        final int row = gameLength / column;

        assert board != null;
        assert boardSpawn != null;

        boardSpawn.post(new Runnable() {
            public void run() {
                boardWidth = boardSpawn.getMeasuredWidth() - 2; //padding!
                boardHeight = boardSpawn.getMeasuredHeight();
                boardSpawn.setClipChildren(false);

                if (!isHorizontal) {
                    boardSpawn.setColumnCount(column);
                    boardSpawn.setRowCount(row);
                } else {
                    boardSpawn.setColumnCount(row);
                    boardSpawn.setRowCount(column);
                }
                boardSpawn.removeAllViews();
                fieldWidth = boardWidth / boardSpawn.getColumnCount();
                fieldHeight = boardHeight / boardSpawn.getRowCount();
                Log.i("Spawner columns count", String.valueOf(boardSpawn.getColumnCount()));
                Log.i("One column width", String.valueOf(fieldWidth));
                Log.i("Spawner rows count", String.valueOf(boardSpawn.getRowCount()));
                Log.i("One column width", String.valueOf(fieldHeight));

                for (int i = 0; i <= gameLength * 9; i++) {
                    FlowLayout cellSpawn = new FlowLayout(GameActivity.this);
                    cellSpawn.setClipChildren(false);

                    if (!isHorizontal) {
                        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams((boardWidth / column), (boardHeight / (row + 1)));
                        cellSpawn.setLayoutParams(params);
                    } else {
                        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams((boardWidth / row), (boardHeight / (column + 1)));
                        cellSpawn.setLayoutParams(params);
                    }
                    boardSpawn.addView(cellSpawn);

                }

                //Adding pawns on starting field
                for (int i = 0; i < playerCount; i++) {
                    Pawn p = playerList.get(i % playerCount).getPawn();
                    View pawn = p.createPawn(playerList, boardSpawn, i, playerCount, getBaseContext());
                    p.addPawn(playerList, boardSpawn, i, playerCount, pawn);
                }
            }
        });

        board.post(new Runnable() {
            public void run() {
                boardWidth = board.getMeasuredWidth() - 2; //padding!
                boardHeight = board.getMeasuredHeight();
                Log.i("Board width", String.valueOf(boardWidth));
                Log.i("Board height", String.valueOf(boardHeight));

                if (!isHorizontal) {
                    board.setColumnCount(column);
                    board.setRowCount(row);
                } else {
                    board.setColumnCount(row);
                    board.setRowCount(column);
                }

                board.removeAllViews();

                Log.i("Board columns count", String.valueOf(board.getColumnCount()));
                Log.i("Board rows count", String.valueOf(board.getRowCount()));

                for (int tilesCount = 0; tilesCount <= gameLength; tilesCount++) {
                    View cInflater = getLayoutInflater().inflate(R.layout.single_cell, null);
                    TextView cell = cInflater.findViewById(R.id.cell);

                    if (!isHorizontal) {
                        cell.setWidth(boardWidth / column);
                        cell.setHeight(boardHeight / (row + 1));
                    } else {
                        cell.setWidth(boardWidth / row);
                        cell.setHeight(boardHeight / (column + 1));
                    }

                    if (tilesCount <= 0)
                        cell.setText(R.string.start);
                    else if (tilesCount >= gameLength)
                        cell.setText(R.string.gameEnd);
                    else
                        cell.setText(String.valueOf(tilesCount));

                    board.addView(cInflater);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Locale current = getResources().getConfiguration().locale;
        String language = String.valueOf(current.getLanguage());

        if (getResources().getBoolean(R.bool.portrait_only))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Find all important views in activity
        final TextView nameDisplay = findViewById(R.id.playerNameView);
        final TextView fieldDisplay = findViewById(R.id.fieldDisplay);
        final android.support.design.widget.FloatingActionButton diceBtn = findViewById(R.id.diceBtn);
        final TextView diceDisplay = findViewById(R.id.diceValue);
        final GridLayout boardSpawn = findViewById(R.id.boardSpawn);
        //SharedPreferences initialization
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        //Get game length from SharedPreferences
        gameLength = Integer.valueOf(sp.getString(Constants.gameLength, "69"));

        //Not needed. For my peace of mind.
        assert nameDisplay != null;
        assert fieldDisplay != null;
        assert diceBtn != null;

        //Populate tasks ArrayList
        final ArrayList<Task> taskList = DatabaseHelper.getDbHelper().getTasksOfLocale(language);

        //Randomize tasks
        randomize(taskList);

        //Populate players ArrayList
        playerList = new ArrayList<>(playerCount);
        populatePlayers(playerCount, playerList);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            if (intent.containsKey(Constants.isGameContinued))
                playerNumber = DatabaseHelper.getDbHelper().getInt(1, Constants.turn, Constants.turn);
            else
                DatabaseHelper.getDbHelper().setKeyTurn(playerNumber);
        }

        nameDisplay.setText(String.valueOf(playerList.get(playerNumber % playerCount).getName()));
        nameDisplay.setTextColor(playerList.get(playerNumber % playerCount).getColor());
        fieldDisplay.setText(String.valueOf(String.valueOf(playerList.get(playerNumber % playerCount).getField())));

        drawBoard();

        diceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!playerChange)
                    play(sp, playerList, fieldDisplay, taskList, diceBtn, diceDisplay, boardSpawn, nameDisplay);
                else
                    changePlayers(diceBtn, nameDisplay, fieldDisplay, diceDisplay);
            }
        });

        diceBtn.setLongClickable(true);
        diceBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getApplicationContext(), "Nice try :P", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}