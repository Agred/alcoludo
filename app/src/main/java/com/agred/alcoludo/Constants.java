package com.agred.alcoludo;

/**
 * Created by agred on 24.01.2017.
 */

public interface Constants {


    String isGameContinued = "isGameContinued";
    String language = "language";
    String gameLength = "gameLength";
    String diceNumber = "diceNumber";
    String diceShape = "diceShape";

    String players = "players";
    String field = "field";
    String color = "color";
    String pawn = "pawn";
    String stats = "stats";
    String turn = "turn";
}
