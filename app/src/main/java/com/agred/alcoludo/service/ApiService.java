package com.agred.alcoludo.service;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.agred.alcoludo.entities.Task;
import com.agred.alcoludo.helpers.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ApiService extends AsyncTask<Object, JSONObject, JSONObject> {

    public static final int ACTION_GET_AVAILABLE_LANGUAGES = 1;
    public static final int ACTION_GET_TASKS_WITH_TRANSLATIONS = 2;
    public static final int ACTION_GET_TRANSLATIONS_COUNT = 3;
    private static final String TAG = "ApiService";
    private static final int API_VERSION = 1;
    private static final String HOST = "http://alcoludo.elektrokom.com.pl/api/v" + ApiService.API_VERSION + "/";
    private ApiAction action;
    private Context context;

    public ApiService(Context context, int actionId) {
        this(context, actionId, new Object[]{});
    }

    public ApiService(Context context, int actionId, Object[] additionalParams) {

        this.context = context;

        switch (actionId) {
            case ACTION_GET_AVAILABLE_LANGUAGES:
                this.action = new ActionGetAvailableLanguages();
                break;
            case ACTION_GET_TASKS_WITH_TRANSLATIONS:
                this.action = new ActionGetTasksWithTranslations();
                break;
            case ACTION_GET_TRANSLATIONS_COUNT:
                this.action = new ActionGetTranslationsCount();
                break;
            default:
                action = null;
        }

        if (action != null) {
            action.setParams(additionalParams);
        }
    }

    @Override
    protected JSONObject doInBackground(Object... objects) {

        if (action == null) {
            return null;
        }

        try {

            HttpURLConnection connection = (HttpURLConnection) new URL(HOST + action.getActionName()).openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String params = action.getParamsAsString();
            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(params);
            writer.flush();
            writer.close();
            os.close();

            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder builder = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            String ret = builder.toString();
            Log.i(TAG, HOST + action.getActionName() + " StringBuilder = " + ret);

            return new JSONObject(ret);

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        //super.onPostExecute(jsonObject); - to check if needed

        if (action.checkIfSuccess(jsonObject)) {
            action.afterExecute(jsonObject);
        }
    }

    private abstract static class ApiAction {

        public abstract String getActionName();

        public abstract void setParams(Object[] params);

        @NonNull
        public abstract String getParamsAsString();

        public final boolean checkIfSuccess(JSONObject o) {

            if (o != null) {
                try {

                    int status = o.getInt("status");
                    if (status == 1) {
                        return true;
                    }

                    Log.e(TAG, "status = 0, error = " + o.getString("error"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        public abstract void afterExecute(JSONObject object);
    }

    /**
     * get available languages action
     * currently isn't defined but may be used for languages other than ones available in application
     */
    private class ActionGetAvailableLanguages extends ApiAction {

        @Override
        public String getActionName() {
            return "get-available-languages";
        }

        @Override
        public void setParams(Object[] params) {
        }

        @NonNull
        @Override
        public String getParamsAsString() {
            return "";
        }

        @Override
        public void afterExecute(JSONObject object) {
        }

    }

    /**
     * get tasks with translations
     */
    private class ActionGetTasksWithTranslations extends ApiAction {

        @Override
        public String getActionName() {
            return "get-tasks-with-translations";
        }

        @Override
        public void setParams(Object[] params) {
        }

        @NonNull
        @Override
        public String getParamsAsString() {
            return "";
        }

        @Override
        public void afterExecute(JSONObject object) {

            try {
                JSONArray jsonTasks = object.getJSONArray("translations");
                ArrayList<Task> tasks = new ArrayList<>();

                for (int i = 0; i < jsonTasks.length(); ++i) {

                    try { // trying again for every task
                        JSONObject jsonTask = jsonTasks.getJSONObject(i);
                        Task task = new Task(
                                jsonTask.getString("task"),
                                jsonTask.getString("language"),
                                jsonTask.getString("actionId"),
                                jsonTask.getString("actionData")
                        );
                        tasks.add(task);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                DatabaseHelper helper = new DatabaseHelper(context);
                helper.resetGame();

                if (tasks.size() > 0) {
                    helper.clearTasksTable();
                    helper.addTasks(tasks);
                    Log.i(TAG, "Database cleared and inserted " + tasks.size() + " rows from API");
                } else if (helper.getTasksCount() == 0) {
                    helper.onCreate(helper.getWritableDatabase());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * get number of translations from web app database
     */
    private class ActionGetTranslationsCount extends ApiAction {

        @Override
        public String getActionName() {
            return "get-translations-count";
        }

        @Override
        public void setParams(Object[] params) {
        }

        @NonNull
        @Override
        public String getParamsAsString() {
            return "";
        }

        @Override
        public void afterExecute(JSONObject object) {
            DatabaseHelper db = new DatabaseHelper(context);
            long localNumberOfTasks = db.getTasksCount();
            int apiNumberOfTasks;
            try {
                apiNumberOfTasks = object.getInt("numberOfTranslations");
            } catch (JSONException e) {
                apiNumberOfTasks = 0;
                e.printStackTrace();
            }

            Log.i(TAG, "localNumberOfTasks = " + localNumberOfTasks + ", apiNumberOfTasks = " + apiNumberOfTasks);

            if (localNumberOfTasks == 0 || localNumberOfTasks < apiNumberOfTasks) {
                Log.i(TAG, "Calling: ApiService.ACTION_GET_TASKS_WITH_TRANSLATIONS");
                new ApiService(context, ApiService.ACTION_GET_TASKS_WITH_TRANSLATIONS).execute();
            }
        }
    }

}
